from pathlib import Path

import numpy as np
import pandas as pd

import files


class FileSplitter:
    splits_overview_df = pd.DataFrame()

    def __init__(self,
                 file_pattern, file_date_format, file_generation_res, files_how_many,
                 data_nominal_res, data_split_duration, indir, outdir):

        self.file_pattern = file_pattern
        self.file_date_format = file_date_format
        self.file_generation_res = file_generation_res
        self.files_how_many = files_how_many
        self.data_nominal_res = data_nominal_res
        self.data_split_duration = data_split_duration
        self.indir = Path(indir)
        self.outdir = Path(outdir)

        self.run()

    def run(self):

        outdirs = files.setup_output_dirs(outdir=self.outdir,
                                          del_previous_results=True)

        files_overview_df = files.FilesDetector(indir=self.indir,
                                                outdir=outdirs['stats'],
                                                file_pattern=self.file_pattern,
                                                file_date_format=self.file_date_format,
                                                file_generation_res=self.file_generation_res,
                                                data_res=self.data_nominal_res,
                                                files_how_many=self.files_how_many).get()

        # Loop through files and their splits
        self.loop_files(files_overview_df=files_overview_df,
                        outdirs=outdirs)

    def loop_files(self, files_overview_df, outdirs):

        for file_idx, file_info_row in files_overview_df.iterrows():

            print(f"\n\nWorking on file '{file_info_row['filepath'].name}'")
            print(f"    Path to file: {file_info_row['filepath']}")

            # Check file availability
            if file_info_row['file_available'] == 0:
                continue

            # Read and prepare data file
            file_df = files.read_raw_data(filepath=file_info_row['filepath'],
                                          nrows=None)

            # Add timestamp to each record
            file_df, true_resolution = files.insert_datetime_index(df=file_df,
                                                                   file_info_row=file_info_row,
                                                                   data_nominal_res=self.data_nominal_res)

            # Add stats to overview
            files_overview_df = files.add_data_stats(df=file_df,
                                                     true_resolution=true_resolution,
                                                     filename=file_info_row['filename'],
                                                     files_overview_df=files_overview_df,
                                                     found_records=len(file_df))

            self.loop_splits(file_df=file_df,
                             file_info_row=file_info_row,
                             outdir_splits=outdirs['splits'])

        files_overview_df.to_csv(outdirs['stats'] / '1_files_stats.csv')
        self.splits_overview_df.to_csv(outdirs['stats'] / '2_splits_stats.csv')

    def loop_splits(self, file_df, file_info_row, outdir_splits):
        counter_splits = -1
        file_df['index'] = pd.to_datetime(file_df.index)

        # Loop segments
        split_grouped = file_df.groupby(pd.Grouper(key='index', freq=self.data_split_duration))
        for split_key, split_df in split_grouped:
            counter_splits += 1
            split_start = split_df.index[0]
            split_end = split_df.index[-1]
            split_name = f"{split_start.strftime('%Y%m%d%H%M%S')}.csv"

            print(f"    Saving split {split_name}")
            split_df.to_csv(outdir_splits / f"{split_name}")

            self.splits_overview_df.loc[split_name, 'start'] = split_start
            self.splits_overview_df.loc[split_name, 'end'] = split_end
            self.splits_overview_df.loc[split_name, 'source_file'] = file_info_row['filepath'].name
            self.splits_overview_df.loc[split_name, 'source_path'] = file_info_row['filepath']

            for col in split_df.columns:
                self.splits_overview_df.loc[split_name, f'numvals_{col}'] = split_df[col].dropna().size
                try:
                    self.splits_overview_df.loc[split_name, f'median_{col}'] = split_df[col].median()
                except TypeError:
                    self.splits_overview_df.loc[split_name, f'median_{col}'] = np.nan

            # # Rotate wind todo
            # wind_rot_df, w_rot_turb_col, scalar_turb_col = \
            #     WindRotation(wind_df=segment_df[[self.u_col, self.v_col, self.w_col, self.scalar_col]],
            #                  u_col=self.u_col, v_col=self.v_col,
            #                  w_col=self.w_col, scalar_col=self.scalar_col).get()


if __name__ == "__main__":
    FileSplitter(file_pattern='201610*1300.csv',  # Accepts regex
                 file_date_format='%Y%m%d%H%M',  # Date format in filename
                 file_generation_res='6H',  # One file expected every x hour(s)
                 data_nominal_res=0.05,  # Measurement every 0.05s
                 files_how_many=1,
                 data_split_duration='30T',  # 30min segments
                 indir=r'A:\FLUXES\CH-DAV\ms - CH-DAV - non-CO2\tests_dynamic_lag\2016_2\converted_raw_data_csv',
                 outdir=r'A:\FLUXES\CH-DAV\ms - CH-DAV - non-CO2\tests_dynamic_lag\FISP_output')
